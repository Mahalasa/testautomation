import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;


import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestDemo {

//    @Test
//    public void displayConfigs() throws IOException, ParseException {
//        JSONParser jsonParser = new JSONParser();
//        Object obj = jsonParser.parse(new FileReader("/Users/mahalasakini/testautomation/testnew.json"));
//
//        JSONObject jsonObject =  (JSONObject) obj;
//
//        Map<String,String> mapss = jsonObject;
//        for(Map.Entry<String, String> ee : mapss.entrySet()){
//            System.out.println("The key are :"+ ee.getKey());
//            System.out.println("The value are :"+ ee.getValue());
//        }
//
//    }

    @Test
    public void readFromGiTLabVariables(){
        String envValues = System.getenv("mockvariables");
        System.out.println("Env Variables:"+ envValues);
        Map<String, String> myMap = new HashMap();
        String valuesRemovingBraces = envValues.replace("{", "").replace("}","");
        String[] pairs = valuesRemovingBraces.split(",");
        for(String parts : pairs){
            String[] subParts = parts.split(":");
            myMap.put(subParts[0], subParts[1]);
        }
        System.out.println("The map is: "+ myMap);

        for(Map.Entry<String, String> ee : myMap.entrySet()){
            String mapKey = ee.getKey().replace("\"", "").replace(" ", "");
            String mapValue = ee.getValue().replace("\"", "").replace(" ", "");
            switch (mapKey){
                case "usi":
                    if(mapValue.equalsIgnoreCase("true")){
                        System.out.println("Usi is enabled");
                    }else{
                        System.out.println("usi is disabled");
                    }
                    break;
                case "mcs":
                    if(mapValue.equalsIgnoreCase("true")){
                        System.out.println("mcs is enabled");
                    }else{
                        System.out.println("mcs is disabled");
                    }
                    break;
                case "ce" :
                    if(mapValue.equalsIgnoreCase("true")){
                        System.out.println("ce is enabled");
                    }else{
                        System.out.println("ce is disabled");
                    }
                    break;
                default:
                    System.out.println("No Mocks found!!!");
            }
        }
    }
}
